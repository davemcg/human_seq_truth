#!/bin/bash
# run this script --partition=quick --mem=64G

module load GATK/3.5-0

me_generated_vcf=$1
test_folder_output=$2
# baseline example:
# /data/mcgaugheyd/projects/nei/mcgaughey/NA12878/truth/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_PGandRTGphasetransfer.SNP.vcf.gz
baseline=$3
# region example
# /data/mcgaugheyd/projects/nei/mcgaughey/NA12878/truth/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_nosomaticdel.bed
# or 
# /data/mcgaugheyd/projects/nei/mcgaughey/NA12878/truth/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_nosomaticdel__Nimblegen_exome_v3_UTR_EZ.b37.bed
region=$4 



# vt normalize my vcf
~/bin/vt_normalize.sh 1000g_b37 $1 ${1%.vcf.gz}_vt.vcf
mv ${1%.vcf.gz}_vt.vcf /scratch/mcgaugheyd/${1%.vcf.gz}_vt.vcf
bgzip /scratch/mcgaugheyd/${1%.vcf.gz}_vt.vcf
tabix -p vcf /scratch/mcgaugheyd/${1%.vcf.gz}_vt.vcf.gz

# extract SNP only vcf
GATK -m 10g SelectVariants \
	-R /fdb/GATK_resource_bundle/b37-2.8/human_g1k_v37_decoy.fasta \
-V /scratch/mcgaugheyd/${1%.vcf.gz}_vt.vcf.gz \
-selectType SNP \
-o /scratch/mcgaugheyd/${1%.vcf.gz}_vt.snp.vcf.gz
tabix -p vcf /scratch/mcgaugheyd/${1%.vcf.gz}_vt.snp.vcf.gz

# extract INDEL only vcf
GATK -m 10g SelectVariants \
	-R /fdb/GATK_resource_bundle/b37-2.8/human_g1k_v37_decoy.fasta \
	-V /scratch/mcgaugheyd/${1%.vcf.gz}_vt.vcf.gz \
	-selectType INDEL \
	-o /scratch/mcgaugheyd/${1%.vcf.gz}_vt.indel.vcf.gz

tabix -p vcf /scratch/mcgaugheyd/${1%.vcf.gz}_vt.indel.vcf.gz
# benchmark SNP
~/bin/rtg-tools-3.7.1/./rtg vcfeval \
	--threads=1 \
	--baseline=$baseline \
	--bed-regions=$region \
	-c /scratch/mcgaugheyd/${1%.vcf.gz}_vt.snp.vcf.gz \
	-o $2_SNP \
	-t ~/bin/rtg-tools-3.7.1/human_g1k_v37_decoy.sdf/

# benchmark INDEL
~/bin/rtg-tools-3.7.1/./rtg vcfeval \
	--threads=1 \
	--baseline=$baseline \
	--bed-regions=$region \
	-c /scratch/mcgaugheyd/${1%.vcf.gz}_vt.indel.vcf.gz \
	-o $2_INDEL \
	-t ~/bin/rtg-tools-3.7.1/human_g1k_v37_decoy.sdf/





