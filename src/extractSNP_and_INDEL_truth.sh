#!/bin/bash
module load GATK/3.5-0

# extract SNP only vcf
GATK -m 20g SelectVariants \
    -R /fdb/GATK_resource_bundle/b37-2.8/human_g1k_v37_decoy.fasta \
    -V /data/mcgaugheyd/projects/nei/mcgaughey/NA12878/truth/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_PGandRTGphasetransfer.vcf.gz \
    --interval_padding 100 \
    -selectType SNP \
    -o /data/mcgaugheyd/projects/nei/mcgaughey/NA12878/truth/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_PGandRTGphasetransfer.SNP.vcf.gz

# extract INDEL only vcf
GATK -m 20g SelectVariants \
    -R /fdb/GATK_resource_bundle/b37-2.8/human_g1k_v37_decoy.fasta \
    -V /data/mcgaugheyd/projects/nei/mcgaughey/NA12878/truth/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_PGandRTGphasetransfer.vcf.gz \
    --interval_padding 100 \
    -selectType INDEL \
    -o /data/mcgaugheyd/projects/nei/mcgaughey/NA12878/truth/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_PGandRTGphasetransfer.INDEL.vcf.gz
