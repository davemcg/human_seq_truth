# biowulf2:/data/mcgaugheyd/projects/nei/mcgaughey/NA12878

### NA12878 Exome, GATK workflow v2
##### https://github.com/davemcg/NGS_genotype_calling/tree/master/NISC_workflow
##### commit: 1bd2578
Threshold|True-pos-baseline|True-pos-call|False-pos|False-neg|Precision|Sensitivity|F-measure|Test
-------|----------|--------|--------|------|------|---------|------------|-----
None|40108|40108|389|2341|0.9904|0.9449|0.9671|SNP
None|3780|3782|897|1057|0.8083|0.7815|0.7947|INDEL

### NA12878 Exome, NISC mpg workflow (Nancy Hansen)
Threshold|True-pos-baseline|True-pos-call|False-pos|False-neg|Precision|Sensitivity|F-measure|Test
-------|----------|--------|--------|------|------|---------|------------|-----
1.000|40183|40183|804|2266|0.9804|0.9466|0.9632|SNP
5.000|3417|3419|969|1420|0.7792|0.7064|0.7410|INDEL

# biowulf2:/data/mcgaugheyd/projects/nei/mcgaughey/NA12878/precisionFDA
#### WGS, 50x precisionFDA HG001/NA12878, GATK workflow v3 (VQSR)
##### https://gitlab.com/davemcg/human_seq_truth/README.md
##### commit: 8f821a55
Threshold|True-pos-baseline|True-pos-call|False-pos|False-neg|Precision|Sensitivity|F-measure|Test
-------|----------|--------|--------|------|------|---------|------------|-----
None|3186592|3186592|3989|22723|0.9987|0.9929|0.9958|SNP
None|476581|476615|4258|4325|0.9911|0.9910|0.9911|INDEL

#### WGS, 35x precisionFDA HG001/NA12878, GATK workflow v3 (VQSR)
Threshold|True-pos-baseline|True-pos-call|False-pos|False-neg|Precision|Sensitivity|F-measure|Test
-------|----------|--------|--------|------|------|---------|------------|-----
None|3203741|3203741|3847|5574|0.9988|0.9983|0.9985|SNP
None|471669|471703|7546|9237|0.9843|0.9808|0.9825|INDEL

#### WGS, 25x precisionFDA HG001/NA12878, GATK workflow v3 (VQSR)
Threshold|True-pos-baseline|True-pos-call|False-pos|False-neg|Precision|Sensitivity|F-measure|Test
-------|----------|--------|--------|------|------|---------|------------|-----
None|3094743|3094743|3677|114572|0.9988|0.9643|0.9813|SNP
None|445454|445487|11902|35452|0.9740|0.9263|0.9495|INDEL

