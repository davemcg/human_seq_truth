### The process to call genotypes from raw sequence (fastq) involves several important decisions:
1. Reference genome (e.g. hg19, GRCh37, g1k_decoy, GRCh38)
2. Aligner (e.g. bwa, bowtie, novoalign)
3. Genotype caller (e.g. samtools, freebayes, GATK)

### Output
The end result is a vcf, which holds the genotype calls. What we want to know is what the precision and sensitivity of a given pipeline. This requires have having a truth set. 

### Truth
The Genome in a Bottle consortium (https://github.com/genome-in-a-bottle/giab_FAQ) has EXTENSIVELY sequenced several samples with just about every sequencing platform and, crucially, has generated a truth vcf with an accompanying bed file to label regions that they have high confidence in. The files are still under active development and are available here: ftp://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/

This will allow use to use Illumina's hap.py (maybe) and RTG's vcfeval tool to compare our vcf calls against the truth call

### Get NA12878 fastq
To test the performance of our genotype calling platform, we use the NIST NA12878/HG001 sequenced samples. Two types: exome and WGS. The WGS is a 50X cut-down version of the NIST HG001 for the precisionFDA challenge. 
```
cd /data/mcgaugheyd/projects/nei/mcgaughey/NA12878/precisionFDA_HG001
bash ~/git/human_seq_truth/src/grab_NA12878_fastq.sh
```

### Align
```
sbatch --cpus-per-task 10 \
	~/git/human_seq_truth/src/run_bwa-mem_hg37d5.sh \
	HG001-NA12878-50x_1.fastq.gz HG001-NA12878-50x_2.fastq.gz \
	\\@RG\\\\tID:HG001_WGS\\\\tSM:HG001_WGS\\\\tPL:ILLUMINA \
	HG001_WGS.bwa-mem.g1k_decoy.bam
```

### Call genotypes to gvcf 
```
# This took just about 10 days. Crazy. 
# git commit: c90f5b7
sbatch --mem=64G --time=240:00:00 ~/bin/exome_workflow_v02/process_and_callGVCF_noB.sh HG001_WGS.bwa-mem.g1k_decoy.bam
```

### Mark PASS/FAIL for genotype calls with hard filtering and VQSR and output vcf
```
# first create gvcfs.list and the ped file
cat gvcfs.list
# HG001_WGS.bwa-mem.g1k_decoy.sorted.markDup.realigned.raw.g.vcf.gz
cat HG001_WGS.ped
# HG001 HG001_WGS 0 0 1 1

# hard filtering
sbatch --mem=20G --time=4:00:00 ~/bin/exome_workflow_v02/GVCF_to_hardFilteredVCF_noB.sh gvcfs.list HG001_WGS.vcf.gz HG001_WGS.ped
# VQSR with 99.97 ts gave best SNP and INDEL performance
sbatch --mem=20G --time=4:00:00 ~/bin/exome_workflow_v03/run_GATK_GVCFtoVCF_withVQSR_v1.sh gvcfs.list HG001_WGS.SNP_ts99.9_AND_INDEL_ts99.97vcf.gz.VQSR_recalibrated_variants.vcf.gz HG001_WGS.ped
```

### Get truth region (bed) and genotypes (vcf)
```
cd /data/mcgaugheyd/projects/nei/mcgaughey/NA12878/truth
wget ftp://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/NA12878_HG001/NISTv3.3.2/GRCh37/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_nosomaticdel.bed 
wget ftp://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/NA12878_HG001/NISTv3.3.2/GRCh37/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_PGandRTGphasetransfer.vcf.gz 
wget ftp://ftp-trace.ncbi.nlm.nih.gov/giab/ftp/release/NA12878_HG001/NISTv3.3.2/GRCh37/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_PGandRTGphasetransfer.vcf.gz.tbi 
```
### Benchmark my genotype call for HG001 with rtg vcfeval
```
# first entry is the vcf
# next is the name of the output folder
# then baseline (answer key) vcf
# finally the region to assay, in bed format
sbatch --mem=64G --partition=quick ~/git/human_seq_truth/src/rtg_vcfeval.sh HG001_WGS.SNP_ts99.9_AND_INDEL_ts99.97vcf.gz.VQSR_recalibrated_variants.vcf.gz \
	SNP_99.9_INDEL_99.97 \
	/data/mcgaugheyd/projects/nei/mcgaughey/NA12878/truth/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_PGandRTGphasetransfer.SNP.vcf.gz \
	/data/mcgaugheyd/projects/nei/mcgaughey/NA12878/truth/HG001_GRCh37_GIAB_highconf_CG-IllFB-IllGATKHC-Ion-10X-SOLID_CHROM1-X_v.3.3.2_highconf_nosomaticdel.bed
```
